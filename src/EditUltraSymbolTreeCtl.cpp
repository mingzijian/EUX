#include "framework.h"

int CreateSymbolTreeCtl( struct TabPage *pnodeTabPage , HFONT hFont )
{
	if( pnodeTabPage->hwndSymbolTree )
	{
		DestroyWindow( pnodeTabPage->hwndSymbolTree );
	}

	/* 创建表列表框 */
	pnodeTabPage->hwndSymbolTree = ::CreateWindow( WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , g_hAppInstance , NULL ) ; 
	// pnodeTabPage->hwndSymbolTree = ::CreateWindow( WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP|TVS_SHOWSELALWAYS , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , g_hAppInstance , NULL ) ; 
	if( pnodeTabPage->hwndSymbolTree == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建符号树控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	::SendMessage( pnodeTabPage->hwndSymbolTree , WM_SETFONT , (WPARAM)hFont, 0);

	::SendMessage( pnodeTabPage->hwndSymbolTree , TVM_SETTEXTCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
	::SendMessage( pnodeTabPage->hwndSymbolTree , TVM_SETLINECOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
	::SendMessage( pnodeTabPage->hwndSymbolTree , TVM_SETBKCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.bgcolor );

	return 0;
}

int GetSymbolTreeItemAndAddTextToEditor( struct TabPage *pnodeTabPage )
{
	HTREEITEM	hti ;
	TVITEM		tvi ;
	char		acTableOrFieldName[ 256 ] ;
	char		*p = NULL ;
	BOOL		bret ;

	hti = TreeView_GetSelection( pnodeTabPage->hwndSymbolTree ) ;
	if( hti == NULL )
		return 1;

	memset( & tvi , 0x00 , sizeof(TVITEM) );
	tvi.mask = TVIF_HANDLE | TVIF_TEXT ;
	tvi.hItem = hti ;
	memset( acTableOrFieldName , 0x00 , sizeof(acTableOrFieldName) );
	tvi.cchTextMax = sizeof(acTableOrFieldName) - 1 ;
	tvi.pszText = acTableOrFieldName ;
	bret = TreeView_GetItem( pnodeTabPage->hwndSymbolTree , & tvi ) ;
	if( bret != TRUE )
		return -1;

	p = strchr( acTableOrFieldName , ' ' ) ;
	if( p )
		*(p) = '\0' ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ADDTEXT , strlen(acTableOrFieldName) , (sptr_t)acTableOrFieldName ) ;

	return 0;
}

int ReloadSymbolTree_SQL( struct TabPage *pnodeTabPage )
{
	char		*acWords2 = NULL ;
	int		nWords2BufferLen ;
	int		nWords2BufferRemainLen ;
	int		nAddWord2Len ;

	char		sql[ 256 ] ;
	MYSQL_RES	*stMysqlResult = NULL ;
	MYSQL_RES	*stMysqlResult2 = NULL ;
	MYSQL_ROW	stMysqlRow ;
	MYSQL_ROW	stMysqlRow2 ;

	TVITEM		tvi ;
	TVINSERTSTRUCT	tvis ;
	HTREEITEM	htiRoot ;

	int		nret = 0 ;

	nret = ConnectToDatabase( pnodeTabPage ) ;
	if( nret < 0 )
	{
		return nret;
	}

	acWords2 = (char*)malloc( SQL_WORD2_BUFFER_SIZE ) ;
	if( acWords2 == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放表名字段名缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acWords2 , 0x00 , SQL_WORD2_BUFFER_SIZE );
	nWords2BufferLen = 0 ;
	nWords2BufferRemainLen = SQL_WORD2_BUFFER_SIZE - 1 ;

	if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "MySQL" ) == 0 )
	{
		struct MySqlFunctions	*pstMySqlFunctions = & (stDatabaseLibraryFunctions.stMysqlFunctions) ;
		struct MySqlHandles	*pstMySqlHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stMysqlHandles) ;

		memset( sql , 0x00 , sizeof(sql) );
		snprintf( sql , sizeof(sql)-1 , "SELECT table_name FROM information_schema.TABLES WHERE table_schema='%s'" , pnodeTabPage->stDatabaseConnectionConfig.dbname );
		nret = pstMySqlFunctions->pfunc_mysql_query( pstMySqlHandles->mysql , sql ) ;
		if( nret )
		{
			::MessageBox(NULL, TEXT("获取数据库表名列表失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( acWords2 );
			return -1;
		}

		stMysqlResult = pstMySqlFunctions->pfunc_mysql_store_result( pstMySqlHandles->mysql ) ;
		if( stMysqlResult )
		{
			TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

			while( stMysqlRow = pstMySqlFunctions->pfunc_mysql_fetch_row(stMysqlResult) )
			{
				memset( & tvi , 0x00 , sizeof(TVITEM) );
				memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
				tvi.mask = TVIF_TEXT ;
				tvi.pszText = stMysqlRow[0] ;
				tvis.hParent = TVI_ROOT ;
				tvis.hInsertAfter = TVI_LAST ;
				tvis.item = tvi;
				htiRoot = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

				nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , stMysqlRow[0] ) ;
				if( nAddWord2Len > 0 )
				{
					nWords2BufferLen += nAddWord2Len ;
					nWords2BufferRemainLen -= nAddWord2Len ;
				}

				memset( sql , 0x00 , sizeof(sql) );
				snprintf( sql , sizeof(sql)-1 , "SELECT column_name,data_type,character_maximum_length,numeric_precision,numeric_scale FROM information_schema.COLUMNS WHERE table_schema='%s' and table_name='%s' ORDER BY ordinal_position ASC" , pnodeTabPage->stDatabaseConnectionConfig.dbname , stMysqlRow[0] );
				nret = pstMySqlFunctions->pfunc_mysql_query( pstMySqlHandles->mysql , sql ) ;
				if( nret )
				{
					::MessageBox(NULL, TEXT("获取数据库字段名列表失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
					free( acWords2 );
					return -1;
				}

				stMysqlResult2 = pstMySqlFunctions->pfunc_mysql_store_result( pstMySqlHandles->mysql ) ;
				if( stMysqlResult2 )
				{
					while( stMysqlRow2 = pstMySqlFunctions->pfunc_mysql_fetch_row(stMysqlResult2) )
					{
						char	buf[ 1024 ] ;
						memset( & tvi , 0x00 , sizeof(TVITEM) );
						memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
						tvi.mask = TVIF_TEXT ;
						memset( buf , 0x00 , sizeof(buf) );
						if( stMysqlRow2[2] )
							snprintf( buf , sizeof(buf)-1 , "%s %s(%s)" , stMysqlRow2[0] , stMysqlRow2[1] , stMysqlRow2[2] );
						else if( stMysqlRow2[3] && stMysqlRow2[4] )
							snprintf( buf , sizeof(buf)-1 , "%s %s(%s,%s)" , stMysqlRow2[0] , stMysqlRow2[1] , stMysqlRow2[3] , stMysqlRow2[4] );
						else
							snprintf( buf , sizeof(buf)-1 , "%s %s" , stMysqlRow2[0] , stMysqlRow2[1] );
						tvi.pszText = buf ;
						tvis.hParent = htiRoot ;
						tvis.hInsertAfter = TVI_LAST ;
						tvis.item = tvi;
						TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis );

						nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , stMysqlRow2[0] ) ;
						if( nAddWord2Len > 0 )
						{
							nWords2BufferLen += nAddWord2Len ;
							nWords2BufferRemainLen -= nAddWord2Len ;
						}
					}
				}

				pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult2 );
			}
		}

		pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult );
	}
	else if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "Oracle" ) == 0 )
	{
		struct OracleFunctions	*pstOracleFunctions = & (stDatabaseLibraryFunctions.stOracleFunctions) ;
		struct OracleHandles	*pstOracleHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stOracleHandles) ;
		sword			swResult ;

		OCIStmt		*stmthpp = NULL ;
		OCIStmt		*stmthpp2 = NULL ;

		OCIDefine	*ocid_table_name = NULL ;
		OCIDefine	*ocid_column_name = NULL ;
		OCIDefine	*ocid_data_type = NULL ;
		OCIDefine	*ocid_data_length = NULL ;
		OCIDefine	*ocid_data_precision = NULL ;
		OCIDefine	*ocid_data_scale = NULL ;

		char		table_name[ 128 + 1 ] ;
		char		column_name[ 128 + 1 ] ;
		char		data_type[ 128 + 1 ] ;
		int		data_length = 0 ;
		int		data_precision = 0 ;
		int		data_scale = 0 ;

		ub2		table_name_len ;
		ub2		column_name_len ;
		ub2		data_type_len ;
		ub2		data_length_len ;
		ub2		data_precision_len ;
		ub2		data_scale_len ;

		sb2		table_name_indicator = 0 ;
		sb2		column_name_indicator = 0 ;
		sb2		data_type_indicator = 0 ;
		sb2		data_length_indicator = 0 ;
		sb2		data_precision_indicator = 0 ;
		sb2		data_scale_indicator = 0 ;

		char sql[ 1024 ] = "SELECT table_name FROM user_tables" ;

		pstOracleFunctions->pfuncOCIHandleAlloc( (dvoid *)(pstOracleHandles->envhpp) , (dvoid **) & stmthpp , OCI_HTYPE_STMT , (size_t)0 , (dvoid **)0 );
		swResult = pstOracleFunctions->pfuncOCIStmtPrepare( stmthpp , pstOracleHandles->errhpp , (text *)sql , (ub4)strlen(sql) , (ub4)OCI_NTV_SYNTAX , (ub4)OCI_DEFAULT ) ;
		if( swResult != OCI_SUCCESS )
		{
			int	nErrorCode ;
			char	acErrorDesc[ 512 ] = "" ;
			GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"构造查询用户表列表失败[%d][%s]" , nErrorCode , acErrorDesc );
			return -1;
		}

		pstOracleFunctions->pfuncOCIDefineByPos( stmthpp , & ocid_table_name , pstOracleHandles->errhpp, 1, (dvoid *)table_name , sizeof(table_name)-1 , SQLT_STR , (void*)table_name_indicator , & table_name_len , NULL , OCI_DEFAULT );

		swResult = pstOracleFunctions->pfuncOCIStmtExecute( pstOracleHandles->svchpp , stmthpp , pstOracleHandles->errhpp , (ub4)1 , (ub4)0 , (OCISnapshot *)NULL , (OCISnapshot *)NULL , OCI_DEFAULT ) ;
		if( swResult != OCI_SUCCESS )
		{
			int	nErrorCode ;
			char	acErrorDesc[ 512 ] = "" ;
			GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行查询用户表列表失败[%d][%s]" , nErrorCode , acErrorDesc );
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		do
		{
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT ;
			tvi.pszText = table_name ;
			tvis.hParent = TVI_ROOT ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			htiRoot = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

			nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , table_name ) ;
			if( nAddWord2Len > 0 )
			{
				nWords2BufferLen += nAddWord2Len ;
				nWords2BufferRemainLen -= nAddWord2Len ;
			}

			memset( sql , 0x00 , sizeof(sql) );
			snprintf( sql , sizeof(sql)-1 , "SELECT column_name,data_type,data_length,data_precision,data_scale FROM user_tab_columns WHERE table_name='%s' ORDER BY column_id ASC" , table_name );

			pstOracleFunctions->pfuncOCIHandleAlloc( (dvoid *)(pstOracleHandles->envhpp) , (dvoid **) & stmthpp2 , OCI_HTYPE_STMT , (size_t)0 , (dvoid **)0 );
			swResult = pstOracleFunctions->pfuncOCIStmtPrepare( stmthpp2 , pstOracleHandles->errhpp , (text *)sql , (ub4)strlen(sql) , (ub4)OCI_NTV_SYNTAX , (ub4)OCI_DEFAULT ) ;
			if( swResult != OCI_SUCCESS )
			{
				int	nErrorCode ;
				char	acErrorDesc[ 512 ] = "" ;
				GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"构造查询用户表字段列表失败[%d][%s]" , nErrorCode , acErrorDesc );
				return -1;
			}

			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_column_name , pstOracleHandles->errhpp, 1, (dvoid *)column_name , sizeof(column_name)-1 , SQLT_STR , (void*)&column_name_indicator , & column_name_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_type , pstOracleHandles->errhpp, 2, (dvoid *)data_type , sizeof(data_type)-1 , SQLT_STR , (void*)&data_type_indicator , & data_type_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_length , pstOracleHandles->errhpp, 3, (dvoid *)&data_length , sizeof(data_length) , SQLT_INT , (void*)&data_length_indicator , & data_length_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_precision , pstOracleHandles->errhpp, 4, (dvoid *)&data_precision , sizeof(data_precision) , SQLT_INT , (void*)&data_precision_indicator , & data_precision_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_scale , pstOracleHandles->errhpp, 5, (dvoid *)&data_scale , sizeof(data_scale) , SQLT_INT , (void*)&data_scale_indicator , & data_scale_len , NULL , OCI_DEFAULT );

			swResult = pstOracleFunctions->pfuncOCIStmtExecute( pstOracleHandles->svchpp , stmthpp2 , pstOracleHandles->errhpp , (ub4)1 , (ub4)0 , (OCISnapshot *)NULL , (OCISnapshot *)NULL , OCI_DEFAULT ) ;
			if( swResult != OCI_SUCCESS )
			{
				int	nErrorCode ;
				char	acErrorDesc[ 512 ] = "" ;
				GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行查询用户表字段列表失败[%d][%s]" , nErrorCode , acErrorDesc );
				return -1;
			}

			do
			{
				char	buf[ 1024 ] ;
				memset( & tvi , 0x00 , sizeof(TVITEM) );
				memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
				tvi.mask = TVIF_TEXT ;
				memset( buf , 0x00 , sizeof(buf) );
				if( data_precision_indicator == -1 )
					snprintf( buf , sizeof(buf)-1 , "%s %s(%d)" , column_name , data_type , data_length );
				else
					snprintf( buf , sizeof(buf)-1 , "%s %s(%d,%d)" , column_name , data_type , data_precision , data_scale );
				tvi.pszText = buf ;
				tvis.hParent = htiRoot ;
				tvis.hInsertAfter = TVI_LAST ;
				tvis.item = tvi;
				TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis );

				nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , column_name ) ;
				if( nAddWord2Len > 0 )
				{
					nWords2BufferLen += nAddWord2Len ;
					nWords2BufferRemainLen -= nAddWord2Len ;
				}
			}
			while( pstOracleFunctions->pfuncOCIStmtFetch2( stmthpp2 , pstOracleHandles->errhpp , 1 , OCI_FETCH_NEXT , 1 , OCI_DEFAULT ) != OCI_NO_DATA );

			pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp2 , OCI_HTYPE_STMT );
		}
		while( pstOracleFunctions->pfuncOCIStmtFetch2( stmthpp , pstOracleHandles->errhpp , 1 , OCI_FETCH_NEXT , 1 , OCI_DEFAULT ) != OCI_NO_DATA );

		pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
	}
	else if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "Sqlite3" ) == 0 )
	{
		struct Sqlite3Functions	*pstSqlite3Functions = & (stDatabaseLibraryFunctions.stSqlite3Functions) ;
		struct Sqlite3Handles	*pstSqlite3Handles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stSqlite3Handles) ;
		char			*errmsg = NULL ;
		char			**result = NULL ;
		int			nrow ;
		int			ncolumn ;
		int			row ;
		int			index ;
		char			**result2 = NULL ;
		int			nrow2 ;
		int			ncolumn2 ;
		int			row2 ;
		int			index2 ;

		memset( sql , 0x00 , sizeof(sql) );
		snprintf( sql , sizeof(sql)-1 , "select name from sqlite_master where type='table' order by name" );
		nret = pstSqlite3Functions->pfunc_sqlite3_get_table( pstSqlite3Handles->sqlite3 , sql , & result , & nrow , & ncolumn , & errmsg ) ;
		if( nret )
		{
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"获取数据库表名列表失败[%s]" , errmsg );
			free( acWords2 );
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		for( row = 0 , index = ncolumn ; row < nrow ; row++ , index++ )
		{
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT ;
			tvi.pszText = result[index] ;
			tvis.hParent = TVI_ROOT ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			htiRoot = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

			nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , result[index] ) ;
			if( nAddWord2Len > 0 )
			{
				nWords2BufferLen += nAddWord2Len ;
				nWords2BufferRemainLen -= nAddWord2Len ;
			}

			memset( sql , 0x00 , sizeof(sql) );
			snprintf( sql , sizeof(sql)-1 , "pragma table_info ('%s')" , result[index] );
			nret = pstSqlite3Functions->pfunc_sqlite3_get_table( pstSqlite3Handles->sqlite3 , sql , & result2 , & nrow2 , & ncolumn2 , & errmsg ) ;
			if( nret )
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"获取数据库字段名列表失败[%s]" , errmsg );
				free( acWords2 );
				pstSqlite3Functions->pfunc_sqlite3_free_table( result ) ;
				return -1;
			}

			for( row2 = 0 , index2 = ncolumn2 ; row2 < nrow2 ; row2++ , index2 += ncolumn2 )
			{
				char	buf[ 1024 ] ;
				memset( & tvi , 0x00 , sizeof(TVITEM) );
				memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
				tvi.mask = TVIF_TEXT ;
				memset( buf , 0x00 , sizeof(buf) );
				snprintf( buf , sizeof(buf)-1 , "%s %s" , result2[index2+1] , result2[index2+2] );
				tvi.pszText = buf ;
				tvis.hParent = htiRoot ;
				tvis.hInsertAfter = TVI_LAST ;
				tvis.item = tvi;
				TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis );

				nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , result2[index2+1] ) ;
				if( nAddWord2Len > 0 )
				{
					nWords2BufferLen += nAddWord2Len ;
					nWords2BufferRemainLen -= nAddWord2Len ;
				}
			}

			pstSqlite3Functions->pfunc_sqlite3_free_table( result2 ) ;
		}

		pstSqlite3Functions->pfunc_sqlite3_free_table( result ) ;
	}
	else if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "PostgreSQL" ) == 0 )
	{
		struct PostgreSQLFunctions	*pstPostgreSQLFunctions = & (stDatabaseLibraryFunctions.stPostgreSQLFunctions) ;
		struct PostgreSQLHandles	*pstPostgreSQLHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stPostgreSQLHandles) ;
		PGresult			*res = NULL ;
		PGresult			*res2 = NULL ;
		int				nrow ;
		int				row ;
		int				nrow2 ;
		int				row2 ;

		memset( sql , 0x00 , sizeof(sql) );
		snprintf( sql , sizeof(sql)-1 , "SELECT table_name FROM information_schema.TABLES WHERE table_catalog='%s' AND table_schema<>'pg_catalog' AND table_schema<>'information_schema' AND table_type='BASE TABLE' ORDER BY table_name ASC" , pnodeTabPage->stDatabaseConnectionConfig.dbname );
		res = pstPostgreSQLFunctions->pfuncPQexec( pstPostgreSQLHandles->postgres , sql ) ;
		if( pstPostgreSQLFunctions->pfuncPQresultStatus(res) != PGRES_TUPLES_OK )
		{
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"获取数据库表名列表失败[%s]" , pstPostgreSQLFunctions->pfuncPQresultErrorMessage(res) );
			free( acWords2 );
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		nrow = pstPostgreSQLFunctions->pfuncPQntuples( res ) ;
		for( row = 0 ; row < nrow ; row++ )
		{
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT ;
			tvi.pszText = pstPostgreSQLFunctions->pfuncPQgetvalue(res,row,0) ;
			tvis.hParent = TVI_ROOT ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			htiRoot = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

			nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , pstPostgreSQLFunctions->pfuncPQgetvalue(res,row,0) ) ;
			if( nAddWord2Len > 0 )
			{
				nWords2BufferLen += nAddWord2Len ;
				nWords2BufferRemainLen -= nAddWord2Len ;
			}

			memset( sql , 0x00 , sizeof(sql) );
			snprintf( sql , sizeof(sql)-1 , "SELECT column_name,data_type,character_maximum_length,numeric_precision,numeric_scale FROM information_schema.COLUMNS WHERE table_catalog='%s' AND table_name='%s' ORDER BY ordinal_position ASC" , pnodeTabPage->stDatabaseConnectionConfig.dbname , pstPostgreSQLFunctions->pfuncPQgetvalue(res,row,0) );
			res2 = pstPostgreSQLFunctions->pfuncPQexec( pstPostgreSQLHandles->postgres , sql ) ;
			if( pstPostgreSQLFunctions->pfuncPQresultStatus(res2) != PGRES_TUPLES_OK )
			{
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"获取数据库字段名列表失败[%s]" , pstPostgreSQLFunctions->pfuncPQresultErrorMessage(res) );
				free( acWords2 );
				pstPostgreSQLFunctions->pfuncPQclear( res ) ;
				return -1;
			}

			nrow2 = pstPostgreSQLFunctions->pfuncPQntuples( res2 ) ;
			for( row2 = 0 ; row2 < nrow2 ; row2++ )
			{
				char	buf[ 1024 ] ;
				memset( & tvi , 0x00 , sizeof(TVITEM) );
				memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
				tvi.mask = TVIF_TEXT ;
				memset( buf , 0x00 , sizeof(buf) );
				if( ! pstPostgreSQLFunctions->pfuncPQgetisnull(res2,row2,2) )
					snprintf( buf , sizeof(buf)-1 , "%s %s(%s)" , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,0) , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,1) , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,2) );
				else
					snprintf( buf , sizeof(buf)-1 , "%s %s(%s,%s)" , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,0) , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,1) , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,3) , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,4) );
				tvi.pszText = buf ;
				tvis.hParent = htiRoot ;
				tvis.hInsertAfter = TVI_LAST ;
				tvis.item = tvi;
				TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis );

				nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , pstPostgreSQLFunctions->pfuncPQgetvalue(res2,row2,0) ) ;
				if( nAddWord2Len > 0 )
				{
					nWords2BufferLen += nAddWord2Len ;
					nWords2BufferRemainLen -= nAddWord2Len ;
				}
			}

			pstPostgreSQLFunctions->pfuncPQclear( res2 ) ;
		}

		pstPostgreSQLFunctions->pfuncPQclear( res ) ;
	}
	else
	{
		AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"数据库类型[%s]暂不支持" , pnodeTabPage->stDatabaseConnectionConfig.dbtype );
		return 0;
	}

	DestroyAutoCompletedShowTree( & (pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree) );
	pnodeTabPage->pstDocTypeConfig->sAutoCompletedBufferSize = 0 ;

	nret = BuildAutoCompletedShowTree( pnodeTabPage->pstDocTypeConfig , pnodeTabPage->pstDocTypeConfig->autocomplete_set ) ;
	if( nret )
		return nret;

	nret = BuildAutoCompletedShowTree( pnodeTabPage->pstDocTypeConfig , acWords2 ) ;
	if( nret )
		return nret;

	nret = ExpandAutoCompletedShowTreeToBuffer( pnodeTabPage->pstDocTypeConfig ) ;
	if( nret )
		return nret;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)acWords2 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );

	free( acWords2 );

	return 0;
}

static HTREEITEM AddReplyToSymbolTree_REDIS( struct TabPage *pnodeTabPage , HTREEITEM hTreeItem , struct redisReply *reply )
{
	char		buf[ 1024 ] ;
	char		*p_buf = NULL ;

	TVITEM		tvi ;
	TVINSERTSTRUCT	tvis ;
	HTREEITEM	hti ;

	HTREEITEM	hret ;

	memset( buf , 0x00 , sizeof(buf) );
	if( reply->type == REDIS_REPLY_STATUS )
		snprintf( buf , sizeof(buf)-1 , "%s (STATUS)" , reply->str );
	else if( reply->type == REDIS_REPLY_ERROR )
		snprintf( buf , sizeof(buf)-1 , "%s (ERROR)" , reply->str );
	else if( reply->type == REDIS_REPLY_STRING )
		snprintf( buf , sizeof(buf)-1 , "%.*s (STRING)" , reply->len , reply->str );
	else if( reply->type == REDIS_REPLY_INTEGER )
		snprintf( buf , sizeof(buf)-1 , "%lld (ERROR)" , reply->integer );
	else if( reply->type == REDIS_REPLY_NIL )
		snprintf( buf , sizeof(buf)-1 , "(null) (NIL)" );
	else if( reply->type == REDIS_REPLY_ARRAY )
		snprintf( buf , sizeof(buf)-1 , "%d (ELEMENTS)" , reply->elements );

	if( pnodeTabPage->nCodePage == ENCODING_UTF8 )
	{
		p_buf = StrdupUtf8ToGb(buf) ;
		if( p_buf == NULL )
			return NULL;
	}
	else
	{
		p_buf = buf ;
	}

	memset( & tvi , 0x00 , sizeof(TVITEM) );
	memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
	tvi.mask = TVIF_TEXT ;
	tvi.pszText = p_buf ;
	tvis.hParent = hTreeItem ;
	tvis.hInsertAfter = TVI_LAST ;
	tvis.item = tvi;
	hti = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

	if( pnodeTabPage->nCodePage == ENCODING_UTF8 )
	{
		free( p_buf );
	}

	if( reply->type == REDIS_REPLY_ARRAY )
	{
		size_t	i ;

		for( i = 0 ; i < reply->elements ; i++ )
		{
			hret = AddReplyToSymbolTree_REDIS( pnodeTabPage , hti , reply->element[i] ) ;
			if( hret == NULL )
				return hret;
		}
	}

	return hti;
}

int DisconnectFromRedis( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage->stRedisConnectionHandles.ctx )
	{
		stRedisLibraryFunctions.pfuncRedisFree( pnodeTabPage->stRedisConnectionHandles.ctx ); pnodeTabPage->stRedisConnectionHandles.ctx = NULL ;
	}

	return 0;
}

int ConnectToRedis( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;
	HTREEITEM	hret ;
	
	if( pnodeTabPage->stRedisConnectionConfig.host[0] == '\0' )
		return 1;

	stRedisLibraryFunctions ;
	pnodeTabPage->stRedisConnectionHandles ;

	if( stRedisLibraryFunctions.hmod_hiredis_dll == NULL )
	{
		stRedisLibraryFunctions.hmod_hiredis_dll = ::LoadLibrary( "hiredis.dll" ) ;
		if( stRedisLibraryFunctions.hmod_hiredis_dll == NULL )
		{
			::MessageBox(NULL, TEXT("不能装载hiredis.dll，请检查是否已安装hiredis以及系统环境变量PATH是否包含hiredis动态链接库文件目录"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		stRedisLibraryFunctions.pfuncRedisConnectWithTimeout = (funcRedisConnectWithTimeout *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "redisConnectWithTimeout" ) ;
		stRedisLibraryFunctions.pfuncRedisFree = (funcRedisFree *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "redisFree" ) ;
		stRedisLibraryFunctions.pfuncRedisCommand = (funcRedisCommand *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "redisCommand" ) ;
		stRedisLibraryFunctions.pfuncFreeReplyObject = (funcFreeReplyObject *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "freeReplyObject" ) ;
		if(	stRedisLibraryFunctions.pfuncRedisConnectWithTimeout == NULL
			|| stRedisLibraryFunctions.pfuncRedisFree == NULL
			|| stRedisLibraryFunctions.pfuncRedisCommand == NULL
			|| stRedisLibraryFunctions.pfuncFreeReplyObject == NULL
			)
		{
			::MessageBox(NULL, TEXT("不能定位函数符号在hiredis.dll"), TEXT("错误"), MB_ICONERROR | MB_OK);
			FreeLibrary( stRedisLibraryFunctions.hmod_hiredis_dll );
			return -1;
		}
	}

	if( pnodeTabPage->stRedisConnectionHandles.ctx == NULL )
	{
		struct timeval		timeout ;
		char			command[ 256 ] ;
		struct redisReply	*reply = NULL ;

		timeout.tv_sec = 10 ;
		timeout.tv_usec = 0 ;
		pnodeTabPage->stRedisConnectionHandles.ctx = stRedisLibraryFunctions.pfuncRedisConnectWithTimeout( pnodeTabPage->stRedisConnectionConfig.host , pnodeTabPage->stRedisConnectionConfig.port , timeout ) ;
		if( pnodeTabPage->stRedisConnectionHandles.ctx == NULL || pnodeTabPage->stRedisConnectionHandles.ctx->err )
		{
			ErrorBox( "不能连接Redis服务器[%s:%d]" , pnodeTabPage->stRedisConnectionConfig.host , pnodeTabPage->stRedisConnectionConfig.port );
			DisconnectFromRedis( pnodeTabPage );
			return -1;
		}

		if( pnodeTabPage->stRedisConnectionConfig.pass[0] )
		{
			memset( command , 0x00 , sizeof(command) );
			snprintf( command , sizeof(command)-1 , "AUTH %s" , pnodeTabPage->stRedisConnectionConfig.pass );
			reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , command ) ;
			if( reply == NULL || reply->type == REDIS_REPLY_ERROR )
			{
				ErrorBox( "向Redis服务器认证失败，请确认密码正确性" );
				DisconnectFromRedis( pnodeTabPage );
				if( reply )
				{
					stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
				}
				return -1;
			}

			stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		}

		if( pnodeTabPage->stRedisConnectionConfig.dbsl[0] )
		{
			memset( command , 0x00 , sizeof(command) );
			snprintf( command , sizeof(command)-1 , "SELECT %s" , pnodeTabPage->stRedisConnectionConfig.dbsl );
			reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , command ) ;
			if( reply == NULL || reply->type == REDIS_REPLY_ERROR )
			{
				ErrorBox( "切换Redis服务器数据桶失败，请确认数据桶配置值正确性" );
				DisconnectFromRedis( pnodeTabPage );
				if( reply )
				{
					stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
				}
				return -1;
			}

			stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		}

		reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , "PING" ) ;
		if( reply == NULL || reply->type == REDIS_REPLY_ERROR )
		{
			ErrorBox( "向Redis服务器发送PING失败" );
			DisconnectFromRedis( pnodeTabPage );
			if( reply )
			{
				stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
			}
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		hret = AddReplyToSymbolTree_REDIS( pnodeTabPage , TVI_ROOT , reply ) ;
		stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		if( hret == NULL )
			return -1;

		TreeView_Expand( pnodeTabPage->hwndSymbolTree , hret , TVE_EXPAND );


		return 1;
	}

	return 0;
}

int ExecuteRedisQuery_REDIS( struct TabPage *pnodeTabPage )
{
	int			nSelStartPos ;
	int			nSelEndPos ;
	int			nSelSqlLength ;
	char			*acSelCommand = NULL ;
	char			*pcSelCommandStatement = NULL ;

	struct redisReply	*reply = NULL ;

	HTREEITEM		hret = NULL ;
	int			nret = 0 ;

	nret = ConnectToRedis( pnodeTabPage ) ;
	if( nret < 0 )
	{
		return nret;
	}

	nSelStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	nSelEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	nSelSqlLength = nSelEndPos - nSelStartPos ;
	if( nSelSqlLength <= 0 )
		return 0;

	acSelCommand = (char*)malloc( nSelSqlLength+1 ) ;
	if( acSelCommand == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放SQL"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acSelCommand , 0x00 , nSelSqlLength+1 );
	GetTextByRange( pnodeTabPage , nSelStartPos , nSelEndPos , acSelCommand );

	pcSelCommandStatement = strtok( acSelCommand , ";" ) ;
	while( pcSelCommandStatement )
	{
		while( (*pcSelCommandStatement) )
		{
			if( (*pcSelCommandStatement) == '\r' || (*pcSelCommandStatement) == '\n' )
				pcSelCommandStatement++;
			else
				break;
		}
		if( pcSelCommandStatement[0] == '\0' )
			break;

		reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , pcSelCommandStatement ) ;
		if( reply == NULL )
		{
			DisconnectFromRedis( pnodeTabPage );
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		hret = AddReplyToSymbolTree_REDIS( pnodeTabPage , TVI_ROOT , reply ) ;
		stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		if( hret == NULL )
			return -1;

		pcSelCommandStatement = strtok( NULL , ";" ) ;
	}

	free( acSelCommand );

	if( hret )
		TreeView_Expand( pnodeTabPage->hwndSymbolTree , hret , TVE_EXPAND );

	return 0;
}

struct ParseTreeStack
{
	HTREEITEM		hti ;

	struct list_head	list_node ;
};

struct ParseTreeContext
{
	struct TabPage		*pnodeTabPage ;

	char			*base ;

	struct list_head	list ;
};

funcCallbackOnXmlNode CallbackOnXmlNode ;
int CallbackOnXmlNode( int type , char *xpath , int xpath_len , int xpath_size , char *node , int node_len , char *properties , int properties_len , char *content , int content_len , void *p )
{
	struct ParseTreeContext	*pstParseXmlContext = (struct ParseTreeContext *) p ;
	struct ParseTreeStack	*pstParentParseXmlStack = list_first_entry( & (pstParseXmlContext->list) , struct ParseTreeStack , list_node ) ;
	struct ParseTreeStack	*pstParseXmlStack = NULL ;

	TVITEM			tvi ;
	TVINSERTSTRUCT		tvis ;
	char			node_buf[ 1024 ] ;
	char			content_buf[ 1024 ] ;
	char			buf[ 1024 ] ;
	char			*p_buf = NULL ;

	if( type & FASTERXML_NODE_BRANCH )
	{
		if( type & FASTERXML_NODE_ENTER )
		{
			pstParseXmlStack = (struct ParseTreeStack *)malloc( sizeof(struct ParseTreeStack) ) ;
			if( pstParseXmlStack == NULL )
				return -1;
			memset( pstParseXmlStack , 0x00 , sizeof(struct ParseTreeStack) );

			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT|TVIF_PARAM ;
			memset( node_buf , 0x00 , sizeof(node_buf) );
			if( node_len > 0 )
				snprintf( node_buf , sizeof(node_buf)-1 , "%.*s" , node_len,node );
			else
				node_buf[0] = '\0' ;
			memset( buf , 0x00 , sizeof(buf) );
			snprintf( buf , sizeof(buf)-1 , "%s" , node_buf );
			if( pstParseXmlContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				p_buf = StrdupUtf8ToGb(buf) ;
				if( p_buf == NULL )
					return -1;
			}
			else
			{
				p_buf = buf ;
			}
			tvi.pszText = p_buf ;
			tvi.lParam = (int)(node-pstParseXmlContext->base) ;
			tvis.hParent = pstParentParseXmlStack->hti ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			pstParseXmlStack->hti = TreeView_InsertItem( pstParseXmlContext->pnodeTabPage->hwndSymbolTree , & tvis ) ;
			TreeView_Expand( pstParseXmlContext->pnodeTabPage->hwndSymbolTree , pstParentParseXmlStack->hti , TVE_EXPAND );

			if( pstParseXmlContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				free( p_buf );
			}

			LIST_ADD( & (pstParseXmlStack->list_node) , & (pstParseXmlContext->list) );
		}
		else if( type & FASTERXML_NODE_LEAVE )
		{
			list_del( & (pstParentParseXmlStack->list_node) );
			free( pstParentParseXmlStack );
		}
		else
		{
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT|TVIF_PARAM ;
			memset( node_buf , 0x00 , sizeof(node_buf) );
			if( node_len > 0 )
				snprintf( node_buf , sizeof(node_buf)-1 , "%.*s" , node_len,node );
			else
				node_buf[0] = '\0' ;
			memset( buf , 0x00 , sizeof(buf) );
			snprintf( buf , sizeof(buf)-1 , "%s" , node_buf );
			if( pstParseXmlContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				p_buf = StrdupUtf8ToGb(buf) ;
				if( p_buf == NULL )
					return -1;
			}
			else
			{
				p_buf = buf ;
			}
			tvi.pszText = p_buf ;
			tvi.lParam = (int)(node-pstParseXmlContext->base) ;
			tvis.hParent = pstParentParseXmlStack->hti ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			TreeView_InsertItem( pstParseXmlContext->pnodeTabPage->hwndSymbolTree , & tvis );
			TreeView_Expand( pstParseXmlContext->pnodeTabPage->hwndSymbolTree , pstParentParseXmlStack->hti , TVE_EXPAND );

			if( pstParseXmlContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				free( p_buf );
			}
		}
	}
	else if( type & FASTERJSON_NODE_LEAF )
	{
		memset( & tvi , 0x00 , sizeof(TVITEM) );
		memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
		tvi.mask = TVIF_TEXT|TVIF_PARAM ;
		memset( node_buf , 0x00 , sizeof(node_buf) );
		if( node_len > 0 )
			snprintf( node_buf , sizeof(node_buf)-1 , "%.*s" , node_len,node );
		else
			node_buf[0] = '\0' ;
		memset( content_buf , 0x00 , sizeof(content_buf) );
		if( content_len > 0 )
			snprintf( content_buf , sizeof(content_buf)-1 , " [%.*s]" , content_len,content );
		else
			content_buf[0] = '\0' ;
		memset( buf , 0x00 , sizeof(buf) );
		snprintf( buf , sizeof(buf)-1 , "%s%s" , node_buf , content_buf );
		if( pstParseXmlContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
		{
			p_buf = StrdupUtf8ToGb(buf) ;
			if( p_buf == NULL )
				return -1;
		}
		else
		{
			p_buf = buf ;
		}
		tvi.pszText = p_buf ;
		tvi.lParam = (int)(node-pstParseXmlContext->base) ;
		tvis.hParent = pstParentParseXmlStack->hti ;
		tvis.hInsertAfter = TVI_LAST ;
		tvis.item = tvi;
		TreeView_InsertItem( pstParseXmlContext->pnodeTabPage->hwndSymbolTree , & tvis );
		TreeView_Expand( pstParseXmlContext->pnodeTabPage->hwndSymbolTree , pstParentParseXmlStack->hti , TVE_EXPAND );

		if( pstParseXmlContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
		{
			free( p_buf );
		}
	}

	return 0;
}

int ReloadSymbolTree_XML( struct TabPage *pnodeTabPage )
{
	int nTextTotalLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
	char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
	if( pcText == NULL )
		return -1;
	memset( pcText , 0x00 , nTextTotalLength + 1 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

	TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

	char xpath[ 1024 ] ;
	struct ParseTreeContext	stParseXmlContext ;
	struct ParseTreeStack	stParseXmlStack ;

	memset( xpath , 0x00 , sizeof(xpath) );
	stParseXmlContext.pnodeTabPage = pnodeTabPage ;
	stParseXmlContext.base = pcText ;
	INIT_LIST_HEAD( & (stParseXmlContext.list) );
	stParseXmlStack.hti = TVI_ROOT ;
	LIST_ADD( & (stParseXmlStack.list_node) , & (stParseXmlContext.list) );
	int nret = TravelXmlBuffer( pcText , xpath , sizeof(xpath)-1 , CallbackOnXmlNode , & stParseXmlContext ) ;

	free( pcText );

	if( nret )
	{
		TVITEM		tvi ;
		TVINSERTSTRUCT	tvis ;
		char		buf[ 64 ] ;
		HTREEITEM	hti ;

		memset( & tvi , 0x00 , sizeof(TVITEM) );
		memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
		tvi.mask = TVIF_TEXT ;
		memset( buf , 0x00 , sizeof(buf) );
		snprintf( buf , sizeof(buf)-1 , "解析失败[%d]" , nret );
		tvi.pszText = buf ;
		tvis.hParent = TVI_ROOT ;
		tvis.hInsertAfter = TVI_LAST ;
		tvis.item = tvi;
		hti = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;
	}

	return 0;
}

funcCallbackOnJsonNode CallbackOnJsonNode ;
int CallbackOnJsonNode( int type , char *jpath , int jpath_len , int jpath_size , char *node , int node_len , char *content , int content_len , void *p )
{
	struct ParseTreeContext	*pstParseJsonContext = (struct ParseTreeContext *) p ;
	struct ParseTreeStack	*pstParentParseJsonStack = list_first_entry( & (pstParseJsonContext->list) , struct ParseTreeStack , list_node ) ;
	struct ParseTreeStack	*pstParseJsonStack = NULL ;

	TVITEM			tvi ;
	TVINSERTSTRUCT		tvis ;
	char			buf[ 1024 ] ;
	char			*p_buf = NULL ;

	if( type & FASTERJSON_NODE_BRANCH )
	{
		if( type & FASTERJSON_NODE_ENTER )
		{
			pstParseJsonStack = (struct ParseTreeStack *)malloc( sizeof(struct ParseTreeStack) ) ;
			if( pstParseJsonStack == NULL )
				return -1;
			memset( pstParseJsonStack , 0x00 , sizeof(struct ParseTreeStack) );

			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT|TVIF_PARAM ;
			if( jpath[0] == '\0' && node_len == 1 && node[0] == '{' )
			{
				strcpy( buf , "(ROOT)" );
			}
			else if( node_len == 0 )
			{
				strcpy( buf , "(NONAME)" );
			}
			else
			{
				memset( buf , 0x00 , sizeof(buf) );
				snprintf( buf , sizeof(buf)-1 , "%.*s" , node_len,node );
			}
			if( pstParseJsonContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				p_buf = StrdupUtf8ToGb(buf) ;
				if( p_buf == NULL )
					return -1;
			}
			else
			{
				p_buf = buf ;
			}
			tvi.pszText = p_buf ;
			tvi.lParam = (int)(node-pstParseJsonContext->base) ;
			tvis.hParent = pstParentParseJsonStack->hti ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			pstParseJsonStack->hti = TreeView_InsertItem( pstParseJsonContext->pnodeTabPage->hwndSymbolTree , & tvis ) ;
			TreeView_Expand( pstParseJsonContext->pnodeTabPage->hwndSymbolTree , pstParentParseJsonStack->hti , TVE_EXPAND );

			if( pstParseJsonContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				free( p_buf );
			}

			LIST_ADD( & (pstParseJsonStack->list_node) , & (pstParseJsonContext->list) );
		}
		else if( type & FASTERJSON_NODE_LEAVE )
		{
			list_del( & (pstParentParseJsonStack->list_node) );
			free( pstParentParseJsonStack );
		}
	}
	else if( type & FASTERJSON_NODE_ARRAY )
	{
		if( type & FASTERJSON_NODE_ENTER )
		{
			pstParseJsonStack = (struct ParseTreeStack *)malloc( sizeof(struct ParseTreeStack) ) ;
			if( pstParseJsonStack == NULL )
				return -1;
			memset( pstParseJsonStack , 0x00 , sizeof(struct ParseTreeStack) );

			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT|TVIF_PARAM ;
			memset( buf , 0x00 , sizeof(buf) );
			snprintf( buf , sizeof(buf)-1 , "%.*s" , node_len,node );
			if( pstParseJsonContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				p_buf = StrdupUtf8ToGb(buf) ;
				if( p_buf == NULL )
					return -1;
			}
			else
			{
				p_buf = buf ;
			}
			tvi.pszText = p_buf ;
			tvi.lParam = (int)(node-pstParseJsonContext->base) ;
			tvis.hParent = pstParentParseJsonStack->hti ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			pstParseJsonStack->hti = TreeView_InsertItem( pstParseJsonContext->pnodeTabPage->hwndSymbolTree , & tvis ) ;
			TreeView_Expand( pstParseJsonContext->pnodeTabPage->hwndSymbolTree , pstParentParseJsonStack->hti , TVE_EXPAND );

			if( pstParseJsonContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
			{
				free( p_buf );
			}

			LIST_ADD( & (pstParseJsonStack->list_node) , & (pstParseJsonContext->list) );
		}
		else if( type & FASTERJSON_NODE_LEAVE )
		{
			list_del( & (pstParentParseJsonStack->list_node) );
			free( pstParentParseJsonStack );
		}
	}
	else if( type & FASTERJSON_NODE_LEAF )
	{
		memset( & tvi , 0x00 , sizeof(TVITEM) );
		memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
		tvi.mask = TVIF_TEXT|TVIF_PARAM ;
		memset( buf , 0x00 , sizeof(buf) );
		snprintf( buf , sizeof(buf)-1 , "%.*s (%.*s)" , node_len,node , content_len,content );
		if( pstParseJsonContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
		{
			p_buf = StrdupUtf8ToGb(buf) ;
			if( p_buf == NULL )
				return -1;
		}
		else
		{
			p_buf = buf ;
		}
		tvi.pszText = p_buf ;
		tvi.lParam = (int)(node-pstParseJsonContext->base) ;
		tvis.hParent = pstParentParseJsonStack->hti ;
		tvis.hInsertAfter = TVI_LAST ;
		tvis.item = tvi;
		TreeView_InsertItem( pstParseJsonContext->pnodeTabPage->hwndSymbolTree , & tvis );
		TreeView_Expand( pstParseJsonContext->pnodeTabPage->hwndSymbolTree , pstParentParseJsonStack->hti , TVE_EXPAND );

		if( pstParseJsonContext->pnodeTabPage->nCodePage == ENCODING_UTF8 )
		{
			free( p_buf );
		}
	}

	return 0;
}

int ReloadSymbolTree_JSON( struct TabPage *pnodeTabPage )
{
	int nTextTotalLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
	char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
	if( pcText == NULL )
		return -1;
	memset( pcText , 0x00 , nTextTotalLength + 1 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

	if( pnodeTabPage->nCodePage == ENCODING_UTF8 )
		g_fasterjson_encoding = FASTERJSON_ENCODING_UTF8 ;
	else if( pnodeTabPage->nCodePage == ENCODING_GBK )
		g_fasterjson_encoding = FASTERJSON_ENCODING_GB18030 ;
	else
		g_fasterjson_encoding = FASTERJSON_ENCODING_UTF8 ;

	TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

	char jpath[ 1024 ] ;
	struct ParseTreeContext	stParseJsonContext ;
	struct ParseTreeStack	stParseJsonStack ;

	memset( jpath , 0x00 , sizeof(jpath) );
	stParseJsonContext.pnodeTabPage = pnodeTabPage ;
	stParseJsonContext.base = pcText ;
	INIT_LIST_HEAD( & (stParseJsonContext.list) );
	stParseJsonStack.hti = TVI_ROOT ;
	LIST_ADD( & (stParseJsonStack.list_node) , & (stParseJsonContext.list) );
	int nret = TravelJsonBuffer( pcText , jpath , sizeof(jpath)-1 , CallbackOnJsonNode , & stParseJsonContext ) ;

	free( pcText );

	if( nret )
	{
		TVITEM		tvi ;
		TVINSERTSTRUCT	tvis ;
		char		buf[ 64 ] ;
		HTREEITEM	hti ;

		memset( & tvi , 0x00 , sizeof(TVITEM) );
		memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
		tvi.mask = TVIF_TEXT ;
		memset( buf , 0x00 , sizeof(buf) );
		snprintf( buf , sizeof(buf)-1 , "解析失败[%d]" , nret );
		tvi.pszText = buf ;
		tvis.hParent = TVI_ROOT ;
		tvis.hInsertAfter = TVI_LAST ;
		tvis.item = tvi;
		hti = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;
	}
	
	return 0;
}

int GetSymbolTreeItemAndGotoEditorPos( struct TabPage *pnodeTabPage )
{
	HTREEITEM	hti ;
	TVITEM		tvi ;
	BOOL		bret ;

	hti = TreeView_GetSelection( pnodeTabPage->hwndSymbolTree ) ;
	if( hti == NULL )
		return 1;

	memset( & tvi , 0x00 , sizeof(TVITEM) );
	tvi.mask = TVIF_HANDLE|TVIF_PARAM ;
	tvi.hItem = hti ;
	bret = TreeView_GetItem( pnodeTabPage->hwndSymbolTree , & tvi ) ;
	if( bret != TRUE )
		return -1;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , tvi.lParam , 0 );
	::SetFocus( pnodeTabPage->hwndScintilla );

	return 0;
}
