#include "framework.h"

int OnSourceCodeBlockFoldVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bBlockFoldVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bBlockFoldVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bBlockFoldVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , MARGIN_FOLD_INDEX , (g_stEditUltraMainConfig.bBlockFoldVisiable==TRUE?16:0) );
	}

	return 0;
}

int OnSourceCodeBlockFoldToggle( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nFoldLevel ;
		int	nFoldHeaderLine ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

		nFoldLevel = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDLEVEL, nCurrentLine, 0 ) ;
		if( nFoldLevel & SC_FOLDLEVELHEADERFLAG )
		{
			nFoldHeaderLine = nCurrentLine ;
		}
		else
		{
			nFoldHeaderLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDPARENT, nCurrentLine, 0 ) ;
		}

		if( nFoldHeaderLine >= 0 )
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nFoldHeaderLine , 0 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_TOGGLEFOLD, nFoldHeaderLine, 0 );
		}
	}

	return 0;
}

int OnSourceCodeBlockFoldContract( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nFoldLevel ;
		int	nFoldHeaderLine ;
		bool	bIsFoldExpand ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

		nFoldLevel = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDLEVEL, nCurrentLine, 0 ) ;
		if( nFoldLevel & SC_FOLDLEVELHEADERFLAG )
		{
			nFoldHeaderLine = nCurrentLine ;
		}
		else
		{
			nFoldHeaderLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDPARENT, nCurrentLine, 0 ) ;
		}

		if( nFoldHeaderLine >= 0 )
		{
			bIsFoldExpand = (bool)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDEXPANDED, nFoldHeaderLine, 0 ) ;
			if( bIsFoldExpand == TRUE )
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nFoldHeaderLine , 0 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_FOLDLINE, nFoldHeaderLine, SC_FOLDACTION_CONTRACT );
			}
		}
	}

	return 0;
}

int OnSourceCodeBlockFoldExpand( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nFoldLevel ;
		int	nFoldHeaderLine ;
		bool	bIsFoldExpand ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

		nFoldLevel = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDLEVEL, nCurrentLine, 0 ) ;
		if( nFoldLevel & SC_FOLDLEVELHEADERFLAG )
		{
			nFoldHeaderLine = nCurrentLine ;
		}
		else
		{
			nFoldHeaderLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDPARENT, nCurrentLine, 0 ) ;
		}

		if( nFoldHeaderLine >= 0 )
		{
			bIsFoldExpand = (bool)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETFOLDEXPANDED, nFoldHeaderLine, 0 ) ;
			if( bIsFoldExpand == FALSE )
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nFoldHeaderLine , 0 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_FOLDLINE, nFoldHeaderLine, SC_FOLDACTION_EXPAND );
			}
		}
	}

	return 0;
}

int OnSourceCodeBlockFoldContractAll( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_FOLDALL, SC_FOLDACTION_CONTRACT, 0 );

	return 0;
}

int OnSourceCodeBlockFoldExpandAll( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_FOLDALL, SC_FOLDACTION_EXPAND, 0 );

	return 0;
}

int OnSetReloadSymbolListOrTreeInterval( struct TabPage *pnodeTabPage )
{
	int		nOldReloadSymbolListOrTreeInterval = g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval ;

	char		nOldReloadSymbolListOrTreeIntervalStr[ 20+1 ] ;

	int		nret = 0 ;

	memset( nOldReloadSymbolListOrTreeIntervalStr , 0x00 , sizeof(nOldReloadSymbolListOrTreeIntervalStr) );
	snprintf( nOldReloadSymbolListOrTreeIntervalStr , sizeof(nOldReloadSymbolListOrTreeIntervalStr)-1 , "%d" , g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval );
_GOTO_RETRY :
	nret = InputBox( g_hwndMainWindow , "请输入定时刷新符号列表或符号树的时间间隔（0~600）（单位：秒）：" , "输入窗口" , 0 , nOldReloadSymbolListOrTreeIntervalStr , sizeof(nOldReloadSymbolListOrTreeIntervalStr)-1 ) ;
	if( nret == IDOK )
	{
		if( nOldReloadSymbolListOrTreeIntervalStr[0] == '\0' || atoi(nOldReloadSymbolListOrTreeIntervalStr) < 0 || atoi(nOldReloadSymbolListOrTreeIntervalStr) > 600 )
		{
			ErrorBox( "输入数值不合法" );
			goto _GOTO_RETRY;
		}

		g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval = atoi(nOldReloadSymbolListOrTreeIntervalStr) ;
		if( nOldReloadSymbolListOrTreeInterval == 0 && g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval > 0 )
		{
			BeginReloadSymbolListOrTreeThread();
		}

		UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );
	}

	return 0;
}

int OnSourceCodeEnableAutoCompletedShow( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bEnableAutoCompletedShow == FALSE )
	{
		g_stEditUltraMainConfig.bEnableAutoCompletedShow = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bEnableAutoCompletedShow = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnSourceCodeAutoCompletedShowAfterInputCharacters( struct TabPage *pnodeTabPage )
{
	char		nAutoCompletedShowAfterInputCharacters[ 2+1 ] ;

	int		nret = 0 ;

	memset( nAutoCompletedShowAfterInputCharacters , 0x00 , sizeof(nAutoCompletedShowAfterInputCharacters) );
	snprintf( nAutoCompletedShowAfterInputCharacters , sizeof(nAutoCompletedShowAfterInputCharacters)-1 , "%d" , g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters );
	nret = InputBox( g_hwndMainWindow , "请输入弹出自动完成框之前输入的字符数：" , "输入窗口" , 0 , nAutoCompletedShowAfterInputCharacters , sizeof(nAutoCompletedShowAfterInputCharacters)-1 ) ;
	if( nret == IDOK )
	{
		if( nAutoCompletedShowAfterInputCharacters[0] )
		{
			g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters = atoi(nAutoCompletedShowAfterInputCharacters) ;

			UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );
		}
	}

	return 0;
}

int OnSourceCodeEnableCallTipShow( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bEnableCallTipShow == FALSE )
	{
		g_stEditUltraMainConfig.bEnableCallTipShow = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bEnableCallTipShow = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

#define DATABASE_CONNECTION_CONFIG	BEGIN_DATABASE_CONNECTION_CONFIG"\r\n" \
					"--  DBTYPE : <MySQL|Oracle|Sqlite3|PostgreSQL>\r\n" \
					"--  DBHOST : <MySQL/PostgreSQL Server IP|(ORACLESID)\r\n" \
					"--  DBPORT : <MySQL/PostgreSQL Port|0>\r\n" \
					"--  DBUSER : <MySQL/PostgreSQL User>\r\n" \
					"--  DBPASS : (MySQL/PostgreSQL Password)\r\n" \
					"--  DBNAME : (MySQL/PostgreSQL Database|Sqlite Path Filename>)\r\n" \
					END_DATABASE_CONNECTION_CONFIG"\r\n" \
					"\r\n"

int OnInsertDataBaseConnectionConfig( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INSERTTEXT , 0 , (sptr_t)DATABASE_CONNECTION_CONFIG );
	}

	return 0;
}

#define REDIS_CONNECTION_CONFIG		BEGIN_REDIS_CONNECTION_CONFIG"\r\n" \
					"--  HOST : 127.0.0.1\r\n" \
					"--  PORT : 6379\r\n" \
					"--  PASS : (DBPASS)\r\n" \
					"--  DBSL : 0\r\n" \
					END_REDIS_CONNECTION_CONFIG"\r\n" \
					"\r\n"

int OnInsertRedisConnectionConfig( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INSERTTEXT , 0 , (sptr_t)REDIS_CONNECTION_CONFIG );
	}

	return 0;
}
